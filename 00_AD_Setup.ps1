# Installajon av AD DS - Kjøres lokalt på maskinen som skal ha installert AD DS (DC1);
# Kjøres som administrator
Install-WindowsFeature AD-Domain-Services, DNS -IncludeManagementTools
$Password = Read-Host -Prompt 'Enter Password' -AsSecureString
Set-LocalUser -Password $Password Administrator
$Params = @{
    DomainMode = 'WinThreshold'
    DomainName = 'bartestad.vgs'
    DomainNetbiosName = 'bartestad'
    ForestMode = 'WinThreshold'
    InstallDns = $true
    NoRebootOnCompletion = $true
    SafeModeAdministratorPassword = $Password
    Force = $true
}
Install-ADDSForest @Params
Restart-Computer
# Logg inn som bartestad\Administrator med passordet ovenfra for å teste domene