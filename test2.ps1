$users = Import-Csv -path 'Users_Final.csv' -Delimiter "," # Fiks path

foreach ($user in $users) {
    $sam = $user.UserPrincipalName.Split("@")
        if ($sam[0].Length -gt 19) {
            "SAM for lang, bruker de 19 første tegnene i variabelen"
            $sam[0] = $sam[0].Substring(0, 19) 
        }
        $sam[0]
        [string] $samaccountname = $sam[0]

        [string] $role = $user.Role
        [string] $searchdn = "OU=$role,OU=$lit_users,*" # Endre $lit_users
        $path = Get-ADOrganizationalUnit -Filter * | Where-Object {($_.name -eq $user.Role) -and ($_.DistinguishedName -like $searchdn)} 

        if (!(Get-ADUser -Filter "sAMAccountName -eq '$($samaccountname)'")) {
            Write-Host "$samaccountname does not exist." -ForegroundColor Green
            Write-Host "Creating User ...." -ForegroundColor Green
            write-Host $user.DisplayName -ForegroundColor Green

            New-ADUser `
            -SamAccountName $samaccountname `
            -UserPrincipalName $user.UserPrincipalName `
            -Name $user.DisplayName `
            -GivenName $user.GivenName `
            -Surname $user.SurName `
            -Enabled $True `
            -ChangePasswordAtLogon $false `
            -DisplayName $user.DisplayName `
            -Role $user.Role `
            -Path $path `
            -AccountPassword (convertto-securestring $user.Password -AsPlainText -Force)

        }
    }


$ADUsers = @()

foreach ($role in $roles) {
    $ADUsers = Get-ADUser -Filter {Role -eq $role} -Properties Role
    #Write-Host "$ADUsers som er funnet under $role"

    foreach ($aduser in $ADUsers) {
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_$role"
    }
}