#Hentet fra gitrepo brukt i forelesningene

$languagelist = Get-WinUserLanguageList
$LanguageList.Add("nb")
Set-WinUserLanguageList $languagelist

Set-WinUserLanguageList -LanguageList nb, en-US -Force

Set-TimeZone -id 'Central Europe Standard Time'
# Sett ExecutionPolicy
# ExecutionPolicy Docs: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
Set-ExecutionPolicy -ExecutionPolicy unrestricted -Scope LocalMachine

# Installere Choco 
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco upgrade chocolatey
# Installere programvare med Choco
choco install -y powershell-core
choco install -y git.install
choco install -y vscode