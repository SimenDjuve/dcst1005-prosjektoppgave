#Koden her generer passord med en funksjon, for så å eksportere dette inn i en ny csv fil
#Så henter koden under csv-filen og putter alle inn i sine respektive grupper


#Funksjon for å generere tilfeldig passord
Function New-UserPassword {
    $chars = [char[]](
        (33..47 | ForEach-Object {[char]$_}) +
        (58..64 | ForEach-Object {[char]$_}) +
        (91..96 | ForEach-Object {[char]$_}) +
        (123..126 | ForEach-Object {[char]$_}) +
        (48..57 | ForEach-Object {[char]$_}) +
        (65..90 | ForEach-Object {[char]$_}) +
        (97..122 | ForEach-Object {[char]$_})
    )

    -join (0..14 | ForEach-Object { $chars | Get-Random })
}


#Funksjon for å formatere csv-fil data til ønsket og godtatt format
function New-UserInfo {
    param (
        [Parameter(Mandatory=$true)][string] $fornavn,
        [Parameter(Mandatory=$true)][string] $etternavn
    )

    if ($fornavn -match $([char]32)) {
        $oppdelt = $fornavn.Split($([char]32))
        $fornavn = $oppdelt[0]

        for ( $index = 1 ; $index -lt $oppdelt.Length ; $index ++ ) {
            $fornavn += ".$($oppdelt[$index][0])"
        }
    }

    $UserPrincipalName = $("$($fornavn).$($etternavn)").ToLower()
    $UserPrincipalName = $UserPrincipalName.Replace('æ','e')
    $UserPrincipalName = $UserPrincipalName.Replace('ø','o')
    $UserPrincipalName = $UserPrincipalName.Replace('å','a')
    $UserPrincipalName = $UserPrincipalName.Replace('é','e')

    Return $UserPrincipalName
}

#Henter data fra csv-fil for så å legge de til i sine respektive grupper
#path må oppdateres
$users = Import-Csv -Path '/Users/simonzahl/Documents/InfrastrukturV23/dcst1005-prosjektoppgave/Users_Final.csv' -Delimiter ";"
$csvfile = @()
$exportuserspath = '/Users/simonzahl/Documents/InfrastrukturV23/dcst1005-prosjektoppgave/Users_path.csv'
$exportpathfinal = '/Users/simonzahl/Documents/InfrastrukturV23/dcst1005-prosjektoppgave/Users_final.csv'

foreach ($user in $users) {
    $password = New-UserPassword
    $line = New-Object -TypeName psobject

    Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $User.GivenName
    Add-Member -InputObject $line -MemberType NoteProperty -Name SurName -Value $user.SurName
    Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName -Value "$(New-UserInfo -Fornavn $user.GivenName -Etternavn $user.SurName)@core.sec"
    Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.surname)" 
    Add-Member -InputObject $line -MemberType NoteProperty -Name department -Value $user.Department
    Add-Member -InputObject $line -MemberType NoteProperty -Name Password -Value $password
    $csvfile += $line
}

$csvfile | Export-Csv -Path $exportuserspath -NoTypeInformation -Encoding 'UTF8'
Import-Csv -Path $exportuserspath | ConvertTo-Csv -NoTypeInformation | ForEach-Object { $_ -Replace '"', ""} | Out-File $exportpathfinal -Encoding 'UTF8'


$users = Import-Csv -path 'Users_Final.csv' -Delimiter ","

foreach ($user in $users) {
    $sam = $user.UserPrincipalName.Split("@")
        if ($sam[0].Length -gt 19) {
            "SAM for lang, bruker de 19 første tegnene i variabelen"
            $sam[0] = $sam[0].Substring(0, 19) 
        }
        $sam[0]
        [string] $samaccountname = $sam[0]

        [string] $department = $user.Department
        [string] $searchdn = "OU=$department,OU=$lit_users,*"
        $path = Get-ADOrganizationalUnit -Filter * | Where-Object {($_.name -eq $user.Department) -and ($_.DistinguishedName -like $searchdn)} 
        
        if (!(Get-ADUser -Filter "sAMAccountName -eq '$($samaccountname)'")) {
            Write-Host "$samaccountname does not exist." -ForegroundColor Green
            Write-Host "Creating User ....%" -ForegroundColor Green
            write-Host $user.DisplayName -ForegroundColor Green

            New-ADUser `
            -SamAccountName $samaccountname `
            -UserPrincipalName $user.UserPrincipalName `
            -Name $user.DisplayName `
            -GivenName $user.GivenName `
            -Surname $user.SurName `
            -Enabled $True `
            -ChangePasswordAtLogon $false `
            -DisplayName $user.DisplayName `
            -Department $user.Department `
            -Path $path `
            -AccountPassword (convertto-securestring $user.Password -AsPlainText -Force)

        }
    }


$ADUsers = @()

foreach ($department in $departments) {
    $ADUsers = Get-ADUser -Filter {Department -eq $department} -Properties Department
    #Write-Host "$ADUsers som er funnet under $department"

    foreach ($aduser in $ADUsers) {
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_$department"
    }

}


#Lag personlig domene admin (passord: Password123!)
$Password = Read-Host -AsSecureString
New-ADUser `
-SamAccountName "test.admin" `
-UserPrincipalName "test.admin@bartestad.vgs" `
-Name "Test Admin" `
-GivenName "Test" `
-Surname "Admin" `
-Enabled $True `
-ChangePasswordAtLogon $false `
-DisplayName "Test Admin" `
-Department "it" `
-Path "OU=it,OU=Bartestad_Users,DC=bartestad,DC=vgs" `
-AccountPassword $Password

Add-ADPrincipalGroupMembership -Identity 'test.admin' -MemberOf "Administrators"
Add-ADPrincipalGroupMembership -Identity 'test.admin' -MemberOf "Domain Admins"



#Flytt PC til rett OU

Move-ADObject -Identity "CN=mgr,CN=Computers,DC=bartestad,DC=vgs" `
            -TargetPath "OU=it,OU=Bartestad_Computers,DC=bartestad,DC=vgs"

New-ADOrganizationalUnit "Servers" `
                -Description "OU for Servers" `
                -Path "OU=Bartestad_Computers,DC=core,DC=sec" `
                -ProtectedFromAccidentalDeletion:$false

#flytter cl1 til it, under OUen computers
Move-ADObject -Identity "CN=cl1,CN=Computers,DC=bartestad,DC=vgs" `
            -TargetPath "OU=it,OU=Bartestad_Computers,DC=bartestad,DC=vgs"

#flytter cl2 til it, under OUen computers
Move-ADObject -Identity "CN=cl2,CN=Computers,DC=bartestad,DC=vgs" `
            -TargetPath "OU=it,OU=Bartestad_Computers,DC=bartestad,DC=vgs"

#flytter cl3 til it, under OUen computers
Move-ADObject -Identity "CN=cl3,CN=Computers,DC=bartestad,DC=vgs" `
            -TargetPath "OU=it,OU=Bartestad_Computers,DC=bartestad,DC=vgs"

#flytter srv1 til it, under OUen computers
Move-ADObject -Identity "CN=srv1,CN=Computers,DC=bartestad,DC=vgs" `
            -TargetPath "OU=it,OU=Bartestad_Computers,DC=bartestad,DC=vgs"

#flytter dc1 til it, under OUen computers
Move-ADObject -Identity "CN=dc1,CN=Computers,DC=bartestad,DC=vgs" `
            -TargetPath "OU=it,OU=Bartestad_Computers,DC=bartestad,DC=vgs"              

