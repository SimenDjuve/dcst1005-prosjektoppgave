#kjøres på mgr (Remote Server Administrative Tools)
Add-WindowsCapability -Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0 -Online
Add-WindowsCapability -Online -Name Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0



# Vår “bedrift” - Bartestad VGS | Oppretter først topOU for users, groups og computers så
# legger den til departments inn i de forskjellige topOU'ene 

$bvgs_users = "Bartestad_Users"
$bvgs_groups = "Bartestad_Groups"
$bvgs_computers = "Bartestad_Computers"

$topOUs = @($bvgs_users,$bvgs_groups,$bvgs_computers )
$departments = @('administration','it','teacher','nurse','student')

foreach ($ou in $topOUs) {
    New-ADOrganizationalUnit $ou -Description "Top OU for Bartestad VGS" -ProtectedFromAccidentalDeletion:$false
    $topOU = Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq "$ou"}
        foreach ($department in $departments) {
            New-ADOrganizationalUnit $department `
                        -Path $topOU.DistinguishedName `
                        -Description "Deparment OU for $department in topOU $topOU" `
                        -ProtectedFromAccidentalDeletion:$false
        }
}




# Gruppe struktur, lager globale grupper for alle departmentene


foreach ($department in $departments) {
    $path = Get-ADOrganizationalUnit -Filter * | 
            Where-Object {($_.name -eq "$department") `
            -and ($_.DistinguishedName -like "OU=$department,OU=$bvgs_groups,*")}
    New-ADGroup -Name "g_$department" `
            -SamAccountName "g_$department" `
            -GroupCategory Security `
            -GroupScope Global `
            -DisplayName "g_$department" `
            -Path $path.DistinguishedName `
            -Description "$department group"
}


# Egen group som skal inneholde alle ansatte

New-ADGroup -name "g_all_employee" `
            -SamAccountName "g_all_employee" `
            -GroupCategory Security `
            -GroupScope Global `
            -DisplayName "g_all_employee" `
            -path "OU=Bartestad_Groups,DC=bartestad,DC=vgs" `
            -Description "all employee"

